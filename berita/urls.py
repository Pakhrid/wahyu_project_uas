from django.urls import path
from berita.views import dashboard, kategori, kategori_add, kategori_update, kategori_delete, artikel, artikel_add, artikel_update, artikel_detail, artikel_delete, pengguna

urlpatterns = [
    path('', dashboard, name='dashboard'),

    path('kategori', kategori, name='kategori'),
    path('kategori/add', kategori_add, name='kategori_add'),
    path('kategori/update/<int:id_kategori>', kategori_update, name='kategori_update'),
    path('kategori/delete/<int:id_kategori>', kategori_delete, name='kategori_delete'),
     
    path('artikel', artikel, name='artikel'),
    path('artikel/add', artikel_add, name='artikel_add'),
    path('artikel/detail/<int:id_artikel>', artikel_detail, name='artikel_detail'),
    path('artikel/update/<int:id_artikel>', artikel_update, name='artikel_update'),
    path('artikel_delete/<int:id_artikel>', artikel_delete, name='artikel_delete'),
path('pengguna', pengguna, name='pengguna'),
]
