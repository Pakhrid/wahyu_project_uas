from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required, user_passes_test

from berita.models import Kategori, Artikel, Pengguna
from berita.forms import ArtikelForm

# Create your views here.
def is_operator(user):
    if user.groups.filter(name='Operator').exists():
        return True
    else:
        return False

@login_required
def dashboard(request):
    template_name = "dashboard/index.html"
    context = {
        'title':'Dashboard'
    }
    return render(request, template_name, context)

@login_required
@user_passes_test(is_operator,login_url='/authentication/logout')
def kategori(request):
    template_name = "dashboard/snippets/kategori.html"
    kategori_item = Kategori.objects.all
    context = {
        'title':'Kategori',
        'kategori':kategori_item
    }
    return render(request, template_name, context)

@login_required
@user_passes_test(is_operator,login_url='/authentication/logout')
def kategori_add(request):
    template_name = "dashboard/snippets/kategori_add.html"
    if request.method == "POST":
        nama = request.POST.get('nama_kategori')
        Kategori.objects.create(
            nama = nama
        )
        return redirect(kategori)
    context = {
        'title':'Tambah Kategori'
    }
    return render(request, template_name, context)

@login_required
@user_passes_test(is_operator,login_url='/authentication/logout')
def kategori_update(request, id_kategori):
    template_name = "dashboard/snippets/kategori_update.html"
    try:
        kategori_input = Kategori.objects.get(id=id_kategori)
    except:
        pass
    if request.method == "POST":
        nama = request.POST.get('nama_kategori')
        kategori_input.nama = nama
        kategori_input.save()
        return redirect(kategori)
    context = {
        'title':'Ubah Kategori',
        'kategori':kategori_input
    }
    return render(request, template_name, context)

@login_required
@user_passes_test(is_operator,login_url='/authentication/logout')
def kategori_delete(request, id_kategori):
    try:
        Kategori.objects.get(id=id_kategori).delete()
    except:
        pass
    return redirect(kategori)

@login_required
def artikel(request):
    template_name = "dashboard/snippets/artikel.html"
    if request.user.groups.filter(name='Operator'):
        artikel = Artikel.objects.all()
    else:
        artikel = Artikel.objects.filter(author=request.user)
    context ={
        'title':'Artikel',
        'artikel':artikel
    }
    return render(request, template_name, context)

@login_required
def artikel_add(request):
    template_name = "dashboard/snippets/artikel_forms.html"
    if request.method == "POST":
        forms = ArtikelForm(request.POST, request.FILES)
        if forms.is_valid():
            pub = forms.save(commit=False)
            pub.author = request.user
            pub.save()
            return redirect(artikel)
    forms = ArtikelForm
    context = {
        'title':'Tambah Artikel',
        'forms':forms
    }
    return render(request, template_name, context)

@login_required
def artikel_detail(request, id_artikel):
    template_name = "dashboard/snippets/artikel_detail.html"
    artikel = Artikel.objects.get(id=id_artikel)
    context = {
        'title': artikel.judul,
        'artikel': artikel
    }
    return render(request, template_name, context)

@login_required
def artikel_update(request, id_artikel):
    template_name = "dashboard/snippets/artikel_forms.html"
    artikel_input = Artikel.objects.get(id=id_artikel)
    if request.user.groups.filter(name='Operator'):
        pass
    else:
        if artikel_input.author != request.user:
            return redirect('/')

    if request.method == "POST":
        forms = ArtikelForm(request.POST, request.FILES, instance=artikel_input)
        if forms.is_valid():
            pub = forms.save(commit=False)
            pub.author = request.user
            pub.save()
            return redirect(artikel)
    forms = ArtikelForm(instance=artikel_input)
    context = {
        'title':'Ubah Artikel',
        'forms':forms
    }
    return render(request, template_name, context)

@login_required  
def artikel_delete(request, id_artikel):
    try:
        artikel = Artikel.objects.get(id=id_artikel)
        if request.user.groups.filter(name='Operator'):
            pass
        else:
            if artikel.author != request.user:
                return redirect('/')
        artikel.delete()
    except:
        pass
    return redirect(artikel)

@login_required
def pengguna(request):
    template_name = "dashboard/snippets/pengguna.html"
    if request.user.groups.filter(name='Operator'):
        pengguna = Pengguna.objects.all()
    else:
        pengguna = Pengguna.objects.filter(author=request.user)
    context ={
        'title':'Artikel',
        'pengguna':pengguna
    }
    return render(request, template_name, context)